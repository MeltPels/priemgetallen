/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package priemgetallen;

import javax.swing.JOptionPane;

public class PriemGetallen {

    public static void main(String[] a) {
        String s;
        int i;
        while (true) {
            // Vraag de gebruiker om input.
            s = JOptionPane.showInputDialog("Geef een getal");
            if (s == null) {
                break;                                                // Als op cancel gedrukt is: stoppen.
            }
            try {
                i = Integer.parseInt(s);

            } catch (NumberFormatException e) {                                 // De Integer.parseInt(s) is mislukt.
                System.out.println("Helaas '" + s + "' is geen integer.");
                continue;                                                       // Spring meteen naar einde van de loop (voor volgende input).
            }

            if (testForPriem(i) == true) {
                System.out.println("De waarde " + i + " is een priemgetal.");
            } else {
                System.out.println("De waarde " + i + " is een gewoon getal.");
            }
        }
        System.out.println("Tot ziens!");
    }

    public static boolean testForPriem(int i) {
        for (int x = 2; x <= i / 2; x++) {
            if (i % x == 0) {
                return false;
            }
        }
        return true;
    }
}
